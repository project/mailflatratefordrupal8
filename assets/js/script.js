// JavaScript Document
jQuery(document).ready(function(e) {
	var delay;
      // Initialize CodeMirror editor with a nice html5 canvas demo.
      var editor = CodeMirror.fromTextArea(document.getElementById('edit-mailflatrate-form-code'), {
        mode: 'text/html',
		lineNumbers: true,
      });
      editor.on("change", function() {
        clearTimeout(delay);
        delay = setTimeout(updatePreview, 300);
      });
      
      function updatePreview() {
     
        document.getElementById('preview').innerHTML=editor.getValue();
      

      }
      setTimeout(updatePreview, 300);
	/* jQuery(".agreetoterms").click(function(e) {
		 			e.preventDefault();
                    jQuery(this).addClass("in-form").removeClass("not-in-form");
					var editor = jQuery(".CodeMirror")[0].CodeMirror; 
					editor.replaceRange('\n<p> <input type="checkbox" class="mailflatrate-label-checkbox agreeterms" id="agreeterms" name="agreeterms"> <label for="agreeterms">do you agree with terms and conditions: </label> \n</p>',CodeMirror.Pos(editor.lastLine()));
                });*/
	jQuery(".submitbuttonadd").click(function(e) {
					e.preventDefault();
                     jQuery(this).addClass("in-form").removeClass("not-in-form");
					var editor = jQuery(".CodeMirror")[0].CodeMirror; 
					editor.replaceRange('\n<p>\n <input type="submit" name="submit" id="submit" value="Sign up">\n </p>',CodeMirror.Pos(editor.lastLine()));
                });
	jQuery(".mailflatrate-label-insert-text").click(function(e) {
					e.preventDefault();
					 jQuery(this).addClass("in-form").removeClass("not-in-form");
 					var editor = jQuery('.CodeMirror')[0].CodeMirror;
					editor.replaceRange('\n<p>\n <label for="'+jQuery(this).attr('htname')+'">'+jQuery(this).val()+': </label> \n<input type="text" id="'+jQuery(this).attr('htname')+'" name="'+jQuery(this).attr('htname')+'" class="mailflatrate-label-text" placeholder="Please enter '+jQuery(this).val()+'">\n</p>\n', CodeMirror.Pos(editor.lastLine()));
				 });
	jQuery(".mailflatrate-label-insert-date").click(function(e) {
					e.preventDefault();
					 jQuery(this).addClass("in-form").removeClass("not-in-form");
 					var editor = jQuery('.CodeMirror')[0].CodeMirror;
					editor.replaceRange('\n<p>\n <label for="'+jQuery(this).attr('htname')+'">'+jQuery(this).val()+': </label> \n<input type="text" id="'+jQuery(this).attr('htname')+'" name="'+jQuery(this).attr('htname')+'" class="mailflatrate-label-date" placeholder="Please enter '+jQuery(this).val()+'">\n</p>\n', CodeMirror.Pos(editor.lastLine()));
				 });
				 jQuery(".mailflatrate-label-insert-datetime").click(function(e) {
					 e.preventDefault();
					 jQuery(this).addClass("in-form").removeClass("not-in-form");
 					var editor = jQuery('.CodeMirror')[0].CodeMirror;
					editor.replaceRange('\n<p>\n <label for="'+jQuery(this).attr('htname')+'">'+jQuery(this).val()+': </label> \n<input type="text" id="'+jQuery(this).attr('htname')+'" name="'+jQuery(this).attr('htname')+'" class="mailflatrate-label-datetime" placeholder="Please enter '+jQuery(this).val()+'">\n</p>\n', CodeMirror.Pos(editor.lastLine()));
				 });
				 jQuery(".mailflatrate-label-insert-textarea").click(function(e) {
					 e.preventDefault();
					 jQuery(this).addClass("in-form").removeClass("not-in-form");
 					var editor = jQuery('.CodeMirror')[0].CodeMirror;
					editor.replaceRange('\n<p> \n <label for="'+jQuery(this).attr('htname')+'">'+jQuery(this).val()+': </label> \n  	<textarea value="" placeholder="Please Enter '+jQuery(this).val()+'" id="'+jQuery(this).attr('htname')+'" class="mailflatrate-label-textarea" name="'+jQuery(this).attr('htname')+'" ></textarea> \n </p>', CodeMirror.Pos(editor.lastLine()));
				 });
				jQuery(".mailflatrate-label-insert-checkbox").click(function(e) {
                    jQuery(this).addClass("in-form").removeClass("not-in-form");
					var editor = jQuery(".CodeMirror")[0].CodeMirror;
					editor.replaceRange('\n<p> <label for="'+jQuery(this).attr('htname')+'"><input type="checkbox" class="mailflatrate-label-checkbox" id="'+jQuery(this).attr('htname')+'" name="'+jQuery(this).attr('htname')+'">'+jQuery(this).attr('htHelpText')+' </label> \n</p>',CodeMirror.Pos(editor.lastLine()));
                });
				jQuery(".mailflatrate-label-insert-country").click(function(e) {
					e.preventDefault();
                    	jQuery(this).addClass("in-form").removeClass("not-in-form");
						var editor = jQuery('.CodeMirror')[0].CodeMirror;
						var htmlStringCountry='';
						htmlStringCountry+='<div class="mailflatrate-countryhidden">\n<p>\n \
 									<label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n \
  									<select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-country">';
 						for(var j=0;j<country.length;j++)
						{
							htmlStringCountry+='<option value="'+country[j]+'">'+country[j]+'</option>';
						}
  						htmlStringCountry+='</select>\n</p></div>';
						editor.replaceRange(htmlStringCountry, CodeMirror.Pos(editor.lastLine()));
                	});
				jQuery(".mailflatrate-label-insert-state").click(function(e) {
					e.preventDefault();
                    	jQuery(this).addClass("in-form").removeClass("not-in-form");
						var editor = jQuery('.CodeMirror')[0].CodeMirror;
						var htmlStringCountry='';
						htmlStringCountry+='<div class="mailflatrate-statehidden">\n<p>\n \
 							<label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n<select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-state">';
  							for(var j=0;j<states.length;j++)
							{
								htmlStringCountry+='<option value="'+states[j]+'">'+states[j]+'</option>';	
							}
  						htmlStringCountry+='</select>\n</p></div>';
						editor.replaceRange(htmlStringCountry, CodeMirror.Pos(editor.lastLine()));
                	});
	jQuery("#edit-mailflatratelist").change(function(e) {
        jQuery.ajax({
			url:document.getElementById("moduleurl").value,
			type:"POST",
			data:{
					list_uid:jQuery(this).val(),
			},
			dataType:"json",
			success: function(records)
			{
				var htmlString='';
				var htmlRequireString='';
				var requiredyes='';
				var requiredyesno='';
				for(i=0;i<records.data.length;i++)
				{
					if(records.data[i].tag!=''  && (records.data[i].type.identifier=='text' ||  records.data[i].type.identifier=='geocountry' || records.data[i].type.identifier=='geostate'))
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-text button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-text button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						}
						
					}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='dropdown')
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							var required_class='required-class';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-dropdown button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						
							htmlRequireString+='<div class="mailflatrate-dropdownhidden">\n<p> \n <label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n  <select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-dropdown '+required_class+'">';
						 	var recordsOptions=Object.keys(records.data[i].options);
						 	for(j=0;j<recordsOptions.length;j++)
							 {
							 	var key=recordsOptions[j];
							 	htmlRequireString+='<option value="'+recordsOptions[j]+'">'+records.data[i].options[key]+'</option>';
						 	 }
							htmlRequireString+='</select>\n </p></div>';
						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							var required_class='required-class';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-dropdown button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						
							htmlString+='<div class="mailflatrate-dropdownhidden">\n<p> \n <label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n  <select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-dropdown '+required_class+'">';
						 	var recordsOptions=Object.keys(records.data[i].options);
						 	for(j=0;j<recordsOptions.length;j++)
							 {
							 	var key=recordsOptions[j];
							 	htmlString+='<option value="'+recordsOptions[j]+'">'+records.data[i].options[key]+'</option>';
						 	}
							htmlString+='</select>\n </p></div>';
						}
						
					}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='checkbox')
					{
						if(records.data[i].help_text!=null)
						{
							var htHelpText=records.data[i].help_text.trim();
							htHelpText=htHelpText.replace(/"/g,"'");
						}
						else 
						{
							var htHelpText=records.data[i].tag.trim();
							htHelpText=htHelpText.replace(/"/g,"'");
						}
						if(records.data[i].label!='')
						{
							if(records.data[i].required=='yes')
							{
								requiredyesno='data-required="yes"';
								requiredyes='in-form';
								htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htHelpText="'+htHelpText+'"  htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkbox button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}
							else
							{
								requiredyesno='data-required="no"';
								requiredyes='not-in-form';
								htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htHelpText="'+htHelpText+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkbox button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}
						}
						else
						{
							if(records.data[i].required=='yes')
							{
								requiredyesno='data-required="yes"';
								requiredyes='in-form';
								htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].tag.trim()+'" htHelpText="'+htHelpText+'"  htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkbox button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}
							else
							{
								requiredyesno='data-required="no"';
								requiredyes='not-in-form';
								htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].tag.trim()+'" htHelpText="'+htHelpText+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkbox button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}
							
						}
											}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='consentcheckbox')
					{
						if(records.data[i].help_text!=null)
						{
							var htHelpText=records.data[i].help_text.trim();
							htHelpText=htHelpText.replace(/"/g,"'");
						}
						else 
						{
							var htHelpText=records.data[i].tag.trim();
							htHelpText=htHelpText.replace(/"/g,"'");
						}
						if(records.data[i].label!='')
						{
							if(records.data[i].required=='yes')
							{
								requiredyesno='data-required="yes"';
								requiredyes='in-form';
								htmlRequireString+='<input '+requiredyesno+' type="button" htHelpText="'+htHelpText+'" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkbox button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}
							else
							{
								requiredyesno='data-required="no"';
								requiredyes='not-in-form';
								htmlString+='<input '+requiredyesno+' htHelpText="'+htHelpText+'" type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkbox button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}
						}
						else
						{
							if(records.data[i].required=='yes')
							{
								requiredyesno='data-required="yes"';
								requiredyes='in-form';
								htmlRequireString+='<input '+requiredyesno+' type="button" htHelpText="'+htHelpText+'" value="'+records.data[i].tag.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkbox button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}
							else
							{
								requiredyesno='data-required="no"';
								requiredyes='not-in-form';
								htmlString+='<input '+requiredyesno+' htHelpText="'+htHelpText+'" type="button" value="'+records.data[i].tag.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkbox button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							}
						}
					}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='multiselect')
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							var required_class='required-class';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-multiselect button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							htmlRequireString+='<div class="mailflatrate-multiselecthidden">\n<p> \n <label for="'+records.data[i].label.trim()+'">'+records.data[i].label.trim()+': </label> \n  <select multiple="multiple" id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-multiselect '+required_class+'">';
						 	var recordsOptions=Object.keys(records.data[i].options);
						 	for(j=0;j<recordsOptions.length;j++)
						 	{
							 	var key=recordsOptions[j];
							 	htmlRequireString+='<option value="'+recordsOptions[j]+'">'+records.data[i].options[key]+'</option>';
						 	}
							htmlRequireString+='</select>\n </p></div>';
						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							var required_class='required-class';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-multiselect button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
							htmlString+='<div class="mailflatrate-multiselecthidden">\n<p> \n <label for="'+records.data[i].label.trim()+'">'+records.data[i].label.trim()+': </label> \n  <select multiple="multiple" id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-multiselect '+required_class+'">';
						 	var recordsOptions=Object.keys(records.data[i].options);
						 	for(j=0;j<recordsOptions.length;j++)
						 	{
							 var key=recordsOptions[j];
							 htmlString+='<option value="'+recordsOptions[j]+'">'+records.data[i].options[key]+'</option>';
						 	}
							htmlString+='</select>\n </p></div>';
						}
						
					}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='date')
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-date button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-date button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

						}
											}	
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='datetime')
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-datetime button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-datetime button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

						}
											}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='textarea')
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-textarea button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-textarea button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

						}
											}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='country')
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							var required_class='required-class';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-country button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';						
						htmlRequireString+='<div class="mailflatrate-countryhidden">\n<p>\n \
 									<label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n \
  									<select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-country '+required_class+'">';
 						for(var j=0;j<country.length;j++)
						{
							htmlRequireString+='<option value="'+country[j]+'">'+country[j]+'</option>';
						}
  						htmlRequireString+='</select>\n</p></div>';
						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							var required_class='required-class';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-country button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';						
						htmlString+='<div class="mailflatrate-countryhidden">\n<p>\n \
 									<label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n \
  									<select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-country '+required_class+'">';
 						for(var j=0;j<country.length;j++)
						{
							htmlString+='<option value="'+country[j]+'">'+country[j]+'</option>';
						}
  						htmlString+='</select>\n</p></div>';
						}
						
					}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='state')
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							var required_class='required-class';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-state button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						htmlRequireString+='<div class="mailflatrate-statehidden">\n<p>\n \
 							<label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n<select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-state '+required_class+'">';
  							for(var j=0;j<states.length;j++)
							{
								htmlRequireString+='<option value="'+states[j]+'">'+states[j]+'</option>';	
							}
  						htmlRequireString+='</select>\n</p></div>';
						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							var required_class='required-class';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-state button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						htmlString+='<div class="mailflatrate-statehidden">\n<p>\n \
 							<label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n<select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-state '+required_class+'">';
  							for(var j=0;j<states.length;j++)
							{
								htmlString+='<option value="'+states[j]+'">'+states[j]+'</option>';	
							}
  						htmlString+='</select>\n</p></div>';
						}
						
					}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='checkboxlist')
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							var required_class='required-class';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkboxlist button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						var recordsOptions=Object.keys(records.data[i].options);
						
						htmlRequireString+='<div class="mailflatrate-checkboxlisthidden">\n<p> \n <label>'+records.data[i].label.trim()+': </label> \n';
						 
						 for(j=0;j<recordsOptions.length;j++)
						 {
							 var key=recordsOptions[j];
							 htmlRequireString+='<input type="checkbox" class="mailflatrate-label-checkboxlist '+required_class+' tag_'+records.data[i].tag.trim()+'" id="'+records.data[i].tag.trim()+'_'+recordsOptions[j]+'" data-checkboxlist="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" value="'+records.data[i].options[key]+'"> <label for="'+records.data[i].tag.trim()+'_'+recordsOptions[j]+'">'+records.data[i].options[key]+'</label>\n';
						 }
						 htmlRequireString+="</div>";
						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							var required_class='required-class';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-checkboxlist button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						var recordsOptions=Object.keys(records.data[i].options);
						
						htmlString+='<div class="mailflatrate-checkboxlisthidden">\n<p> \n <label>'+records.data[i].label.trim()+': </label> \n';
						 
						 for(j=0;j<recordsOptions.length;j++)
						 {
							 var key=recordsOptions[j];
							 htmlString+='<input type="checkbox" class="mailflatrate-label-checkboxlist '+required_class+' tag_'+records.data[i].tag.trim()+'" id="'+records.data[i].tag.trim()+'_'+recordsOptions[j]+'" data-checkboxlist="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" value="'+records.data[i].options[key]+'"> <label for="'+records.data[i].tag.trim()+'_'+recordsOptions[j]+'">'+records.data[i].options[key]+'</label>\n';
						 }
						 htmlString+="</div>";
						}
						
					}
					else if(records.data[i].tag!='' && records.data[i].type.identifier=='radiolist')
					{
						if(records.data[i].required=='yes')
						{
							requiredyesno='data-required="yes"';
							requiredyes='in-form';
							var required_class='required-class';
							htmlRequireString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-radiolist button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						var recordsOptions=Object.keys(records.data[i].options);
						
						htmlRequireString+='<div class="mailflatrate-radiolisthidden">\n<p> \n <label>'+records.data[i].label.trim()+': </label> \n';
						 
						 for(j=0;j<recordsOptions.length;j++)
						 {
							 var key=recordsOptions[j];
							  htmlRequireString+='<input type="radio" class="mailflatrate-radio required-class tag_'+records.data[i].tag.trim()+'" data-radiolist="'+records.data[i].tag.trim()+'" id="'+recordsOptions[j]+'" name="'+records.data[i].tag.trim()+'" value="'+records.data[i].options[key]+'"> <label for="'+recordsOptions[j]+'">'+records.data[i].options[key]+'</label>\n';
						 }
						 htmlRequireString+="</div>";
						}
						else
						{
							requiredyesno='data-required="no"';
							requiredyes='not-in-form';
							var required_class='required-class';
							htmlString+='<input '+requiredyesno+' type="button" value="'+records.data[i].label.trim()+'" htname="'+records.data[i].tag.trim()+'" id="id_'+records.data[i].tag.trim()+'" class="mailflatrate-label-insert-radiolist button '+requiredyes+'" name="name_'+records.data[i].tag.trim()+'" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
						var recordsOptions=Object.keys(records.data[i].options);
						
						htmlString+='<div class="mailflatrate-radiolisthidden">\n<p> \n <label>'+records.data[i].label.trim()+': </label> \n';
						 
						 for(j=0;j<recordsOptions.length;j++)
						 {
							 var key=recordsOptions[j];
							  htmlString+='<input type="radio" class="mailflatrate-radio required-class tag_'+records.data[i].tag.trim()+'" data-radiolist="'+records.data[i].tag.trim()+'" id="'+recordsOptions[j]+'" name="'+records.data[i].tag.trim()+'" value="'+records.data[i].options[key]+'"> <label for="'+recordsOptions[j]+'">'+records.data[i].options[key]+'</label>\n';
						 }
						 htmlString+="</div>";
						}
						
					}
				}
					//htmlRequireString+='<button class="button in-form" type="button" value="0">Submit button</button>\
						//<button class="button not-in-form agreetoterms" type="button"  value="3">Agree to terms</button>';
				htmlString+='<button class="button in-form" type="button" value="0">'+jQuery("#submitbuttonhidden").val()+'</button>';
				jQuery(".available-fields.small-margin.not-required .buttons-fields-mailflatrate").html(htmlString);
				jQuery(".available-fields.small-margin.required .buttons-fields-mailflatrate").html(htmlRequireString);


				jQuery(".submitbuttonadd").click(function(e) {
					e.preventDefault();
                     jQuery(this).addClass("in-form").removeClass("not-in-form");
					var editor = jQuery(".CodeMirror")[0].CodeMirror; 
					editor.replaceRange('\n<p>\n <input type="submit" name="submit" id="submit" value="Sign up">\n </p>',CodeMirror.Pos(editor.lastLine()));
                });
				/*jQuery(".agreetoterms").click(function(e) {
					e.preventDefault();
                     jQuery(this).addClass("in-form").removeClass("not-in-form");
					var editor = jQuery(".CodeMirror")[0].CodeMirror; 
					editor.replaceRange('\n<p> <input type="checkbox" class="mailflatrate-label-checkbox agreeterms required-class" id="agreeterms" name="agreeterms"> <label for="agreeterms">do you agree with terms and conditions: </label> \n</p>',CodeMirror.Pos(editor.lastLine()));
                });*/
				 jQuery(".mailflatrate-label-insert-text").click(function(e) {
					 e.preventDefault();
					 jQuery(this).addClass("in-form").removeClass("not-in-form");
 					var editor = jQuery('.CodeMirror')[0].CodeMirror;
					var datarequired=jQuery(this).data('required');
					var requiredData1='';
					var datarequireclass='';
					if(datarequired=='yes') { datarequireclass='required-class';requiredData1='required';}
					editor.replaceRange('\n<p>\n <label for="'+jQuery(this).attr('htname')+'">'+jQuery(this).val()+': </label> \n<input type="text" id="'+jQuery(this).attr('htname')+'" name="'+jQuery(this).attr('htname')+'" class="mailflatrate-label-text '+datarequireclass+'" '+requiredData1+' placeholder="Please enter '+jQuery(this).val()+'">\n</p>\n', CodeMirror.Pos(editor.lastLine()));
				 });
				 jQuery(".mailflatrate-label-insert-dropdown").click(function(e) {
					 e.preventDefault();
                    	jQuery(this).addClass("in-form").removeClass("not-in-form");
						var editor = jQuery('.CodeMirror')[0].CodeMirror;
						var datarequired=jQuery(this).data('required');
						editor.replaceRange(jQuery(".mailflatrate-dropdownhidden").html(), CodeMirror.Pos(editor.lastLine()));
                	});
					
					jQuery(".mailflatrate-label-insert-checkboxlist").click(function(e) {
						e.preventDefault();
                    	jQuery(this).addClass("in-form").removeClass("not-in-form");
						var editor = jQuery('.CodeMirror')[0].CodeMirror;
						
						editor.replaceRange(jQuery(".mailflatrate-checkboxlisthidden").html(), CodeMirror.Pos(editor.lastLine()));
                	});
					jQuery(".mailflatrate-label-insert-country").click(function(e) {
						e.preventDefault();
                    	jQuery(this).addClass("in-form").removeClass("not-in-form");
						var editor = jQuery('.CodeMirror')[0].CodeMirror;
						editor.replaceRange(jQuery(".mailflatrate-countryhidden").html(), CodeMirror.Pos(editor.lastLine()));
                	});
					jQuery(".mailflatrate-label-insert-state").click(function(e) {
						e.preventDefault();
                    	jQuery(this).addClass("in-form").removeClass("not-in-form");
						var editor = jQuery('.CodeMirror')[0].CodeMirror;
						editor.replaceRange(jQuery(".mailflatrate-statehidden").html(), CodeMirror.Pos(editor.lastLine()));
                	});
				jQuery(".mailflatrate-label-insert-checkbox").click(function(e) {
					e.preventDefault();
                    jQuery(this).addClass("in-form").removeClass("not-in-form");
					var editor = jQuery(".CodeMirror")[0].CodeMirror;
					editor.replaceRange('\n<p> <input type="checkbox" class="mailflatrate-label-checkbox" id="'+jQuery(this).attr('htname')+'" name="'+jQuery(this).attr('htname')+'"> <label for="'+jQuery(this).attr('htname')+'">'+jQuery(this).attr('htHelpText')+' </label> \n</p>',CodeMirror.Pos(editor.lastLine()));
                });
				jQuery(".mailflatrate-label-insert-multiselect").click(function(e) {
					e.preventDefault();
                    jQuery(this).addClass("in-form").removeClass("not-in-form");
						var editor = jQuery('.CodeMirror')[0].CodeMirror;
						editor.replaceRange(jQuery(".mailflatrate-multiselecthidden").html(), CodeMirror.Pos(editor.lastLine()));
                });
				jQuery(".mailflatrate-label-insert-radiolist").click(function(e) {
					e.preventDefault();
                    jQuery(this).addClass("in-form").removeClass("not-in-form");
						var editor = jQuery('.CodeMirror')[0].CodeMirror;
						editor.replaceRange(jQuery(".mailflatrate-radiolisthidden").html(), CodeMirror.Pos(editor.lastLine()));
                });
				jQuery(".mailflatrate-label-insert-date").click(function(e) {
					e.preventDefault();
					 jQuery(this).addClass("in-form").removeClass("not-in-form");
 					var editor = jQuery('.CodeMirror')[0].CodeMirror;
					editor.replaceRange('\n<p>\n <label for="'+jQuery(this).attr('htname')+'">'+jQuery(this).val()+': </label> \n<input type="text" id="'+jQuery(this).attr('htname')+'" name="'+jQuery(this).attr('htname')+'" class="mailflatrate-label-date" placeholder="Please enter '+jQuery(this).val()+'">\n</p>\n', CodeMirror.Pos(editor.lastLine()));
				 });
				 jQuery(".mailflatrate-label-insert-datetime").click(function(e) {
					 e.preventDefault();
					 jQuery(this).addClass("in-form").removeClass("not-in-form");
 					var editor = jQuery('.CodeMirror')[0].CodeMirror;
					editor.replaceRange('\n<p>\n <label for="'+jQuery(this).attr('htname')+'">'+jQuery(this).val()+': </label> \n<input type="text" id="'+jQuery(this).attr('htname')+'" name="'+jQuery(this).attr('htname')+'" class="mailflatrate-label-datetime" placeholder="Please enter '+jQuery(this).val()+'">\n</p>\n', CodeMirror.Pos(editor.lastLine()));
				 });
				 jQuery(".mailflatrate-label-insert-textarea").click(function(e) {
					 e.preventDefault();
					 jQuery(this).addClass("in-form").removeClass("not-in-form");
 					var editor = jQuery('.CodeMirror')[0].CodeMirror;
					editor.replaceRange('\n<p> \n <label for="'+jQuery(this).attr('htname')+'">'+jQuery(this).val()+': </label> \n  	<textarea required value="" placeholder="Please Enter '+jQuery(this).val()+'" id="'+jQuery(this).attr('htname')+'" class="mailflatrate-label-textarea" name="'+jQuery(this).attr('htname')+'" ></textarea> \n </p>', CodeMirror.Pos(editor.lastLine()));
				 });
				 if(jQuery("#add_auto_to_editor").is(":checked"))
				 {
				htmlStringTextarea='';
				for(i=0;i<records.data.length;i++)
				{
					if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='text')
					{
						
						htmlStringTextarea+='\n<p> \n <label for="'+records.data[i].label.trim()+'">'+records.data[i].label.trim()+': </label> \n  	<input type="text" value="" placeholder="Please Enter '+records.data[i].label.trim()+'" id="'+records.data[i].tag.trim()+'" class="mailflatrate-label-input required-class" name="'+records.data[i].tag.trim()+'" /> \n </p>';
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='dropdown')
					{
						htmlStringTextarea+='\n<p> \n <label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n  <select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-dropdown required-class">';
						 var recordsOptions=Object.keys(records.data[i].options);
						 for(j=0;j<recordsOptions.length;j++)
						 {
							 var key=recordsOptions[j];
							 htmlStringTextarea+='<option value="'+recordsOptions[j]+'">'+records.data[i].options[key]+'</option>';
						 }
						htmlStringTextarea+='</select>\n </p>';
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes'&& records.data[i].type.identifier=='checkbox')
					{
						if(records.data[i].help_text!=null)
						{
							var htHelpText=records.data[i].help_text.trim();
							htHelpText=htHelpText.replace(/"/g,"'");
						}
						else 
						{
							var htHelpText=records.data[i].tag.trim();
							htHelpText=htHelpText.replace(/"/g,"'");
						}
						htmlStringTextarea+='\n<p> <label for="'+records.data[i].tag.trim()+'"> <input type="checkbox" class="mailflatrate-label-checkbox required-class" id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'"> '+htHelpText+' </label> \n</p>';
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes'&& records.data[i].type.identifier=='consentcheckbox')
					{
						if(records.data[i].help_text!=null)
						{
							var htHelpText=records.data[i].help_text.trim();
							htHelpText=htHelpText.replace(/"/g,"'");
						}
						else 
						{
							var htHelpText=records.data[i].tag.trim();
							htHelpText=htHelpText.replace(/"/g,"'");
						}
						htmlStringTextarea+='\n<p>  <label for="'+records.data[i].tag.trim()+'"> <input type="checkbox" class="mailflatrate-label-checkbox required-class" id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'">'+htHelpText+' </label> \n</p>';
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='multiselect')
					{
						htmlStringTextarea+='\n<p> \n <label for="'+records.data[i].label.trim()+'">'+records.data[i].label.trim()+': </label> \n  <select multiple="multiple" id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-multiselect required-class">';
						 var recordsOptions=Object.keys(records.data[i].options);
						 for(j=0;j<recordsOptions.length;j++)
						 {
							 var key=recordsOptions[j];
							 htmlStringTextarea+='<option value="'+recordsOptions[j]+'">'+records.data[i].options[key]+'</option>';
						 }
						htmlStringTextarea+='</select>\n </p>';
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='date')
					{
						htmlStringTextarea+='\n<p> \n <label for="'+records.data[i].label.trim()+'">'+records.data[i].label.trim()+': </label> \n  	<input type="text"  value="" placeholder="Please Enter '+records.data[i].label.trim()+'" id="'+records.data[i].tag.trim()+'" class="mailflatrate-label-date required-class" name="'+records.data[i].tag.trim()+'" /> \n </p>';
						
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='datetime')
					{
						htmlStringTextarea+='\n<p> \n <label for="'+records.data[i].label.trim()+'">'+records.data[i].label.trim()+': </label> \n  	<input type="text"  value="" placeholder="Please Enter '+records.data[i].label.trim()+'" id="'+records.data[i].tag.trim()+'" class="mailflatrate-label-datetime required-class" name="'+records.data[i].tag.trim()+'" /> \n </p>';
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='textarea')
					{
						htmlStringTextarea+='\n<p> \n <label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n  	<textarea  value="" placeholder="Please Enter '+records.data[i].label.trim()+'" id="'+records.data[i].tag.trim()+'" class="mailflatrate-label-textarea required-class" name="'+records.data[i].tag.trim()+'" ></textarea> \n </p>';
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='country')
					{
						htmlStringTextarea+='\n<p>\n \
 											<label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n \
  											<select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-country required-class">';
 						for(var j=0;j<country.length;j++)
						{
							htmlStringTextarea+='<option value="'+country[j]+'">'+country[j]+'</option>';
						}
  						htmlStringTextarea+='</select>\n</p>';
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='state')
					{
							htmlStringTextarea+='\n<p>\n \
 							<label for="'+records.data[i].tag.trim()+'">'+records.data[i].label.trim()+': </label> \n<select id="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" class="mailflatrate-label-state required-class">';
  							for(var j=0;j<states.length;j++)
							{
								htmlStringTextarea+='<option value="'+states[j]+'">'+states[j]+'</option>';	
							}
  							htmlStringTextarea+='</select>\n</p>';
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='checkboxlist')
					{
						var recordsOptions=Object.keys(records.data[i].options);
						
						htmlStringTextarea+='\n<p> \n <label>'+records.data[i].label.trim()+': </label> \n ';
						 
						 for(j=0;j<recordsOptions.length;j++)
						 { 	
							 var key=recordsOptions[j];
							  htmlString+='<input type="checkbox" class="mailflatrate-label-checkboxlist '+required_class+' tag_'+records.data[i].tag.trim()+'" id="'+records.data[i].tag.trim()+'_'+recordsOptions[j]+'" data-checkboxlist="'+records.data[i].tag.trim()+'" name="'+records.data[i].tag.trim()+'" value="'+records.data[i].options[key]+'"> <label for="'+records.data[i].tag.trim()+'_'+recordsOptions[j]+'">'+records.data[i].options[key]+'</label>\n';
						 }
					}
					else if(records.data[i].tag!='' && records.data[i].required=='yes' && records.data[i].type.identifier=='radiolist')
					{
						var recordsOptions=Object.keys(records.data[i].options);
						
						htmlStringTextarea+='\n<p> \n <label>'+records.data[i].label.trim()+': </label> \n';
						 
						 for(j=0;j<recordsOptions.length;j++)
						 {
							 var key=recordsOptions[j];
							 htmlStringTextarea+='<input type="radio" class="mailflatrate-radio required-class tag_'+records.data[i].tag.trim()+'" data-radiolist="'+records.data[i].tag.trim()+'" id="'+recordsOptions[j]+'" name="'+records.data[i].tag.trim()+'" value="'+records.data[i].options[key]+'"> <label for="'+recordsOptions[j]+'">'+records.data[i].options[key]+'</label>\n';
						 }
					}
				}
				htmlsubmitbutton='<p><input type="submit" name="submit" id="submit" value="'+jQuery("#signuptext").val()+'"></p>';
				jQuery("#edit-mailflatrate-form-code").html(htmlStringTextarea+htmlsubmitbutton);
				jQuery(".CodeMirror").remove();
				var delay;
      // Initialize CodeMirror editor with a nice html5 canvas demo.
      var editor = CodeMirror.fromTextArea(document.getElementById('edit-mailflatrate-form-code'), {
        mode: 'text/html',
		lineNumbers: true,
      });
      editor.on("change", function() {
        clearTimeout(delay);
        delay = setTimeout(updatePreview, 300);
      });
     
      function updatePreview() {
        document.getElementById('preview').innerHTML=editor.getValue();
		
      }
      setTimeout(updatePreview, 300);
	  }
			} 
		});
    });
});
