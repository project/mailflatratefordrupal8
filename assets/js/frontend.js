jQuery(document).ready(function(e) {
    jQuery("#mailflatrate-form-submit").submit(function(e) {
        e.preventDefault();
	jQuery('html, body').animate({ scrollTop: 0 }, 'slow', function () {
    });
	jQuery("#overlay").css("display","block");
		jQuery.ajax({
			url:document.getElementById("submitmoduleurl").value,
			data:jQuery(this).serialize(),
			type:"POST",
			success: function(data)
			{
			jQuery("#overlay").css("display","none");
				data=jQuery.parseJSON(data);
				if(data.status==='success')
				{
					jQuery("form#mailflatrate-form-submit").css("display","none");
					jQuery(".messagebox").css("color",data.color);
					jQuery(".messagebox").addClass("success");
					jQuery(".messagebox").html(data.message);
				}
				else if(data.status==='error')
				{
					jQuery(".messagebox").addClass("errormsg");
					jQuery(".messagebox").css("color",data.color);
					jQuery(".messagebox").html(data.message);
				}
			}
		});
    });
});