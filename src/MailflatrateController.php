<?php
/**
 * @file
 * Contains \Drupal\mailflatrate\MailflatrateController.
 */

namespace Drupal\mailflatrate;


use Drupal\Core\Controller\ControllerBase;


class MailflatrateController extends ControllerBase {
  public function content() {
  	global $language;
 	$config = $this->config('mailflatrate.settings');
 	$configEditor = $this->config('mailflatrate.editor_form');
    $publickey  = $config->get('mailflatrate.manilflatrate_publickey');
	$privatekey  = $config->get('mailflatrate.manilflatrate_privatekey');
	$list_uid  = $configEditor->get('mailflatrate.mailflatratelist');
   	$lang_name = (isset($language->language))?$language->language:'';
	$queryArray=array();
	foreach($_POST as $key => $value)
	{
		if(strcmp($key,'action')!=0)
		{
			$queryArray[$key]=$value;
		}
	}
	$configMessages = $this->config('mailflatrate.message_form');
    $mailflatrate_successfully_subscribed=$configMessages->get('mailflatrate.mailflatrate-successfully-subscribeed');
    $subscribed_color=(strcmp($configMessages->get('mailflatrate.mailflatrate-successfully-subscribeed-color'),'')!=0)?$config->get('mailflatrate.mailflatrate-successfully-subscribeed-color'):'#000000';
    if((strcmp($mailflatrate_successfully_subscribed,'')!=0))
    {
      $mailflatrate_successfully_subscribed=$configMessages->get('mailflatrate.mailflatrate-successfully-subscribeed');
    }
    else
    {
      $mailflatrate_successfully_subscribed=$this->t('Thanks for your subscription. You will receive a confirmation email in minutes.');
    }

    $invalid_email_color=$configMessages->get('mailflatrate.mailflatrate-invalid-email-address-color');
		if((strcmp($invalid_email_color,'')!=0))
		{
			$invalid_email_color=$configMessages->get('mailflatrate.mailflatrate-invalid-email-address-color');
		}
		else
		{
			$invalid_email_color='#000000';
		}

		$invalid_email_address=$configMessages->get('mailflatrate.mailflatrate-invalid-email-address');

		if((strcmp($invalid_email_address,'')!=0))
		{
			$invalid_email_address=$configMessages->get('mailflatrate.mailflatrate-invalid-email-address');
		}
		else
		{
			$invalid_email_address=$this->t('Your email is invalid. Please check it again.');
		}

		$already_subscribe_color=$configMessages->get('mailflatrate.mailflatate_already_subscribed-color');
		if((strcmp($already_subscribe_color,'')!=0))
		{
			$already_subscribe_color=$configMessages->get('mailflatrate.mailflatate_already_subscribed-color');
		}
		else
		{
			$already_subscribe_color='#000000';
		}


		$already_subscribe_text=$configMessages->get('mailflatrate.mailflatate_already_subscribed');

		if((strcmp($already_subscribe_text,'')!=0))
		{
			$already_subscribe_text=$configMessages->get('mailflatrate.mailflatate_already_subscribed');
		}
		else
		{
			$already_subscribe_text=$this->t('This email address is already subscribed.');
		}


		 $missing_email_address_color=$configMessages->get('mailflatrate.missing-email-address-color');
		if((strcmp($missing_email_address_color,'')!=0))
		{
			$missing_email_address_color=$configMessages->get('mailflatrate.missing-email-address-color');
		}
		else
		{
			$missing_email_address_color='#000000';
		}


		$missing_email_address=$configMessages->get('mailflatrate.missing-email-address');

		if((strcmp($missing_email_address,'')!=0))
		{
			$missing_email_address=$configMessages->get('mailflatrate.missing-email-address');
		}
		else
		{
			$missing_email_address=$this->t('Please provide the subscriber email address');
		}
		
		
		$data_protection_color=$configMessages->get('mailflatrate.data_protection-color');
		if((strcmp($missing_email_address_color,'')!=0))
		{
			$data_protection_color=$configMessages->get('mailflatrate.data_protection-color');
		}
		else
		{
			$data_protection_color='#000000';
		}


		$data_protection=$configMessages->get('mailflatrate.data-protection');

		if((strcmp($data_protection,'')!=0))
		{
			$data_protection=$configMessages->get('mailflatrate.data-protection');
		}
		else
		{
			$data_protection=$this->t('Please provide the data protection');
		}

		mailflatrate_ajax_submit($publickey,$privatekey,$list_uid,$queryArray,$mailflatrate_successfully_subscribed,$already_subscribe_text,$missing_email_address,$invalid_email_address,$subscribed_color,$already_subscribe_color,$missing_email_address_color,$invalid_email_color,$data_protection,$data_protection_color);


	
	die;
   drupal_exit();
  }

  public function getlistajax()
  {
  	$config = $this->config('mailflatrate.settings');
    $publickey  = $config->get('mailflatrate.manilflatrate_publickey');
	$privatekey  = $config->get('mailflatrate.manilflatrate_privatekey');
	
	$response=mailflatrate_remote_ajax($publickey,$privatekey,$_POST['list_uid']);
	if(isset($response->body['data']['records']))
		{
			echo json_encode(array('data'=>$response->body['data']['records'],'msg'=>'found')); 
		}
		else
		{
			echo json_encode(array('data'=>array(),'msg'=>'not found'));
		}
		die;
  	drupal_exit();
  }
}