<?php
/**
 * @file
 * Contains \Drupal\mailflatrate\EditorsForm.
 */

namespace Drupal\mailflatrate;
 
use Drupal\Core\Form\ConfigFormBase;
 
use Drupal\Core\Form\FormStateInterface;

class MessageForm extends ConfigFormBase {

	public function getFormId() {
 
    	return 'mailflatrate_config_message_form';
 
  	}
  	public function buildForm(array $form, FormStateInterface $form_state) {
		 $lang_name=\Drupal::languageManager()->getCurrentLanguage()->getId(); 
    	$form = parent::buildForm($form, $form_state);
    	$config = $this->config('mailflatrate.message_form');

		$form['#attached']['library'][] = 'mailflatrate/settings-massage';
		$subscribed_color=(strcmp($config->get('mailflatrate.mailflatrate-successfully-subscribeed-color'),'')!=0)?$config->get('mailflatrate.mailflatrate-successfully-subscribeed-color'):'#000000';
		$form['mailflatrate-successfully-subscribeed-color']=array(
			'#type' => 'textfield',
			'#default_value' => $subscribed_color
  		);
  		$subscribed_mailflatrate_successfully=$config->get('mailflatrate.mailflatrate-successfully-subscribeed');

  		if((strcmp($subscribed_mailflatrate_successfully,'')!=0))
  		{
  			$subscribed_mailflatrate_successfully=$config->get('mailflatrate.mailflatrate-successfully-subscribeed');
  		 
  		}
  		else
  		{
  			$subscribed_mailflatrate_successfully=$this->t('Thanks for your subscription. You will receive a confirmation email in minutes.');
  		}


		   $form['mailflatrate-successfully-subscribeed']=array(
	'#type' => 'textfield',
	'#title' => $this->t('Successfully subscribed'),
    '#description' => $this->t('The text that shows when an email address is successfully subscribed to the selected list(s).'),
    '#required' => TRUE,
	'#default_value' => $subscribed_mailflatrate_successfully
  );
		$invalid_email_color=$config->get('mailflatrate.mailflatrate-invalid-email-address-color');
		if((strcmp($invalid_email_color,'')!=0))
		{
			$invalid_email_color=$config->get('mailflatrate.mailflatrate-invalid-email-address-color');
		}
		else
		{
			$invalid_email_color='#000000';
		}

	 	$form['mailflatrate-invalid-email-address-color']=array(
				'#type' => 'textfield',
				'#prefix' => '<br>',
				'#default_value' => $invalid_email_color
  		);
		$invalid_email_address=$config->get('mailflatrate.mailflatrate-invalid-email-address');

		if((strcmp($invalid_email_address,'')!=0))
		{
			$invalid_email_address=$config->get('mailflatrate.mailflatrate-invalid-email-address');
		}
		else
		{
			$invalid_email_address=$this->t('Your email is invalid. Please check it again.');
		}

	 	$form['mailflatrate-invalid-email-address']=array(
				'#type' => 'textfield',
				'#title' => $this->t('Invalid email address'),
    			'#description' => $this->t('The text that shows when an invalid email address is given.'),
    			'#required' => TRUE,
				'#default_value' => $invalid_email_address
  			); 


	 	$already_subscribe_color=$config->get('mailflatrate.mailflatate_already_subscribed-color');
		if((strcmp($already_subscribe_color,'')!=0))
		{
			$already_subscribe_color=$config->get('mailflatrate.mailflatate_already_subscribed-color');
		}
		else
		{
			$already_subscribe_color='#000000';
		}

	 	$form['mailflatate_already_subscribed-color']=array(
	'#type' => 'textfield',
	'#prefix' => '<br>',
	'#default_value' => $already_subscribe_color
  );


	 	$already_subscribe_text=$config->get('mailflatrate.mailflatate_already_subscribed');

		if((strcmp($already_subscribe_text,'')!=0))
		{
			$already_subscribe_text=$config->get('mailflatrate.mailflatate_already_subscribed');
		}
		else
		{
			$already_subscribe_text=$this->t('This email address is already subscribed.');
		}

	 	 $form['mailflatate_already_subscribed']=array(
				'#type' => 'textfield',
				'#title' => $this->t('Already subscribed'),
    			'#description' =>  $this->t('The text that shows when the given email is already subscribed to the selected list(s).'),
    			'#required' => TRUE,
				'#default_value' => $already_subscribe_text
  		);


	 	 $mailflatrate_agree_to_term_color=$config->get('mailflatrate.mailflatrate-agree_to_terms-color');
		if((strcmp($mailflatrate_agree_to_term_color,'')!=0))
		{
			$mailflatrate_agree_to_term_color=$config->get('mailflatrate.mailflatrate-agree_to_terms-color');
		}
		else
		{
			$mailflatrate_agree_to_term_color='#000000';
		}


	 	  $form['mailflatrate-agree_to_terms-color']=array(
				'#type' => 'textfield',
				'#prefix' => '<br>',
				'#default_value' => $mailflatrate_agree_to_term_color
  			);

		$mailflatrate_agree_to_term=$config->get('mailflatrate.mailflatrate-agree_to_terms');

		if((strcmp($mailflatrate_agree_to_term,'')!=0))
		{
			$mailflatrate_agree_to_term=$config->get('mailflatrate.mailflatrate-agree_to_terms');
		}
		else
		{
			$mailflatrate_agree_to_term=$this->t('Please agree with terms and conditions');
		}

	 	  $form['mailflatrate-agree_to_terms']=array(
	'#type' => 'textfield',
	'#title' => $this->t('Agree to terms'),
    '#description' => $this->t('The text that shows when the checkbox isn´t checked.'),
    '#required' => TRUE,
	'#default_value' => $mailflatrate_agree_to_term
  );

	 	  $missing_email_address_color=$config->get('mailflatrate.missing-email-address-color');
		if((strcmp($missing_email_address_color,'')!=0))
		{
			$missing_email_address_color=$config->get('mailflatrate.missing-email-address-color');
		}
		else
		{
			$missing_email_address_color='#000000';
		}

	 	   $form['missing-email-address-color']=array(
	'#type' => 'textfield',
	'#prefix' => '<br>',
	'#default_value' => $missing_email_address_color
  );

	 	   $missing_email_address=$config->get('mailflatrate.missing-email-address');

		if((strcmp($missing_email_address,'')!=0))
		{
			$missing_email_address=$config->get('mailflatrate.missing-email-address');
		}
		else
		{
			$missing_email_address=$this->t('Please provide the subscriber email address');
		}

	 	    $form['missing-email-address']=array(
	'#type' => 'textfield',
	'#title' => $this->t('Missing email address'),
    '#description' => $this->t('This text appears if the e-mail was not specified.'),
    '#required' => TRUE,
	'#default_value' => $missing_email_address
  );
  
  $data_protection_color=$config->get('mailflatrate.data-protection-color');
		if((strcmp($data_protection_color,'')!=0))
		{
			$data_protection_color=$config->get('mailflatrate.data-protection-color');
		}
		else
		{
			$data_protection_color='#000000';
		}
		
  	 $form['data-protection-color']=array(
	'#type' => 'textfield',
	'#prefix' => '<br>',
	'#default_value' => $data_protection_color
  );

	 	   $data_protection=$config->get('mailflatrate.data-protection');

		if((strcmp($data_protection,'')!=0))
		{
			$data_protection=$config->get('mailflatrate.data-protection');
		}
		else
		{
			$data_protection=$this->t('Please provide the data protection');
		}

	 	    $form['data-protection']=array(
	'#type' => 'textfield',
	'#title' => $this->t('Data protection'),
    '#description' => $this->t('This text appears if the data protection was not specified.'),
    '#required' => TRUE,
	'#default_value' => $data_protection
  );
  

    return $form;
	}
	 public function submitForm(array &$form, FormStateInterface $form_state) {
 
    $config = $this->config('mailflatrate.message_form');
 
    $config->set('mailflatrate.mailflatrate-successfully-subscribeed-color', $form_state->getValue('mailflatrate-successfully-subscribeed-color'));

	$config->set('mailflatrate.mailflatrate-successfully-subscribeed', $form_state->getValue('mailflatrate-successfully-subscribeed'));

	$config->set('mailflatrate.mailflatrate-invalid-email-address-color', $form_state->getValue('mailflatrate-invalid-email-address-color'));

	$config->set('mailflatrate.mailflatrate-invalid-email-address', $form_state->getValue('mailflatrate-invalid-email-address'));

	$config->set('mailflatrate.mailflatate_already_subscribed-color', $form_state->getValue('mailflatate_already_subscribed-color'));

	$config->set('mailflatrate.mailflatate_already_subscribed', $form_state->getValue('mailflatate_already_subscribed'));

	$config->set('mailflatrate.mailflatrate-agree_to_terms-color', $form_state->getValue('mailflatrate-agree_to_terms-color'));

	$config->set('mailflatrate.mailflatrate-agree_to_terms', $form_state->getValue('mailflatrate-agree_to_terms'));

	$config->set('mailflatrate.missing-email-address-color', $form_state->getValue('missing-email-address-color'));

	$config->set('mailflatrate.missing-email-address', $form_state->getValue('missing-email-address'));
 	
	$config->set('mailflatrate.data-protection-color', $form_state->getValue('data-protection-color'));

	$config->set('mailflatrate.data-protection', $form_state->getValue('data-protection'));
	
    $config->save();
 
    return parent::submitForm($form, $form_state);
 
  }
	protected function getEditableConfigNames() {
 
    return [
 
      'mailflatrate.message_form',
 
    ];
 
  }
}
?>