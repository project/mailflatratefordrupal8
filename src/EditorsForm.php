<?php
/**
 * @file
 * Contains \Drupal\mailflatrate\EditorsForm.
 */

namespace Drupal\mailflatrate;
 
use Drupal\Core\Form\ConfigFormBase;
 
use Drupal\Core\Form\FormStateInterface;


class EditorsForm extends ConfigFormBase {
 /**  
   * {@inheritdoc}  
   */  
  public function getFormId() {
 
    return 'mailflatrate_config_editor_form';
 
  }

  /**
 
   * {@inheritdoc}
 
   */
 public function buildForm(array $form, FormStateInterface $form_state) {
	 
  $lang_name=\Drupal::languageManager()->getCurrentLanguage()->getId();
    $form = parent::buildForm($form, $form_state);
 	//$form['#attached']['css'][] = drupal_get_path('module', 'mailflatrate') . '/assets/css/mailflatratestyle.css';
	$config = $this->config('mailflatrate.editor_form');
	$configSettings = $this->config('mailflatrate.settings');
	
  $publickey  = $configSettings->get('mailflatrate.manilflatrate_publickey');
	$privatekey  = $configSettings->get('mailflatrate.manilflatrate_privatekey');
	$Lists=getList($publickey,$privatekey,1,20000);
	if(isset($Lists->body['data']['records']))
		{
			if(count($Lists->body['data']['records']) > 0)
			{
	  			$options = array(
        			'select' => $this->t('Select')
    			);
				foreach($Lists->body['data']['records'] as $list)
				{
					$options[$list['general']['list_uid']]=$list['general']['name'];
				}
				$customHtmlstartList='<div class="wrap mfr-settings">';
				$form['agreetotermhidden'] = array(
        			'#type' => 'hidden',
					'#prefix' => $customHtmlstartList, 
        			'#default_value' => $this->t('Agree to terms'),
					'#attributes' => array('id'=>'agreetotermhidden') 
    			);
    			 $customHtmlstartList='';
    			 $form['submitbuttonhidden'] = array(
        			'#type' => 'hidden',
					
        			'#default_value' => $this->t('Submit'),
					'#attributes' => array('id'=>'submitbuttonhidden') 
    			);
    			  $form['doyouagreetext'] = array(
        			'#type' => 'hidden',
					
        			'#default_value' => $this->t('do you agree with terms and conditions'),
					'#attributes' => array('id'=>'doyouagreetext') 
    			);
    			   $form['signuptext'] = array(
        			'#type' => 'hidden',
        			'#default_value' => $this->t('Sign up'),
					'#attributes' => array('id'=>'signuptext') 
    			);
				
					$customHtmlstartList='<div class="row">
					<div class="main-content col col-4">
					<h1 class="page-title"></h1>
					<div class="clear"></div>
					<h1>'.$this->t('Form').'</h1>
					<div id="titlediv" class="small-margin">';
			$form['moduleurl'] = array(
        		'#type' => 'hidden',
				'#prefix' => $customHtmlstartList, 
        		'#default_value' => base_path().'admin/config/mailflatrate/getlistajax',
				'#attributes' => array('id'=>'moduleurl') 
    		);
           $customHtmlstartList='<div>
             </div>
                </div><div class="mailflatrate-tabs">';
				$form['mailflatratelist'] = array(
        		'#type' => 'select',
        		'#options' => $options,
				'#prefix' => $customHtmlstartList,
        		'#title' => $this->t('Select Mailflatrate List'),
        		'#default_value' => $config->get('mailflatrate.mailflatratelist')
    		);
			$germanOrEnglish=$this->t('Add required fields to editor automatically');
			$customHtmlstart='<div class="small-margin available-fields">'; 
      

        $form['add_auto_to_editor'] = array(
  '#type' => 'checkbox',
  '#prefix' => $customHtmlstart,
  '#title' => $germanOrEnglish,
  '#id' => 'add_auto_to_editor',
  '#attributes' => array('checked'=>'checked')
);
        $customHtmlstart='</div><div class="available-fields small-margin not-required"><h4>'.((strcmp($lang_name,'de')==0)?'Wählen Sie ein Feld aus, das Sie dem Formular hinzufügen möchten':'Choose a field to add to the form').'</h4><div class="tiny-margin"><strong>'.$this->t('Form fields').'</strong><div class="buttons-fields-mailflatrate">';
		if(strcmp($config->get('mailflatrate.mailflatratelist'),'select')!=0 && strcmp($config->get('mailflatrate.mailflatratelist'),'')!=0)
							{
								$responseObject=getListFields($config->get('mailflatrate.mailflatratelist'));
								if(isset($responseObject->body['data']['records']))
								{
									$records=$responseObject->body['data']['records'];
									$requiredyes='yes';
									$requiredyesno='no';
									foreach($records as $record)
									{
										if(strcmp($record['required'],'yes')!=0)
										{

										$requiredyesno=(strcmp($record['required'],'yes')==0)?'yes':'no';

										if((strcmp($record['type']['identifier'],'text')==0 || strcmp($record['type']['identifier'],'geocountry')==0 || strcmp($record['type']['identifier'],'geostate')==0) && strcmp($record['label'],'')!=0)
										{
										$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-text button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											); 
										$customHtmlstart='';
										//$customHtmlstart.='<input type="button"'.$requiredyesno.' value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-text button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
								   }
								  		else if(strcmp($record['type']['identifier'],'dropdown')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-dropdown button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
                                        $customHtmlstart='<div class="mailflatrate-dropdownhidden">\n<p> \n <label for="'.$record['tag'].'">'.$record['label'].' : </label> \n';
                                        $customHtmlend='</p>
                                        </div>';
                                        $options = array(
        									'select' => $this->t('Select')
    									);

                                        foreach($record['options'] as $key => $option)
											{
												$options[$key] = $option;
												
											}

                        $form[$record['tag']] = array(
        												'#type' => 'select',
        												'#options' => $options,
														    '#prefix' => $customHtmlstart,
														    '#suffix' => $customHtmlend,
        												'#attributes' => array('class' => array("mailflatrate-label-dropdown ".(strcmp($record['required'],'yes')==0)?'required-class':'')) 
    										);
    										$customHtmlstart='';
    										$customHtmlend='';
										 }
										else if(strcmp($record['type']['identifier'],'checkbox')==0)
										{
											if(strcmp($record['label'],'')!=0)
											{
												$form['name_'.$record['tag']] = array(
													'#type' => 'button',
													'#prefix' => $customHtmlstart,
													'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
													'#value' => $this->t($record['label']),
													'#attributes' => array('class'=>array('mailflatrate-label-insert-checkbox button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag'],'htHelpText'=>str_replace('"',"'",$record['help_text'])),
													'#id' => 'id_'.$record['tag'],
												);
											}
											else
											{
												$form['name_'.$record['tag']] = array(
													'#type' => 'button',
													'#prefix' => $customHtmlstart,
													'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
													'#value' => $this->t($record['tag']),
													'#attributes' => array('class'=>array('mailflatrate-label-insert-checkbox button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag'],'htHelpText'=>str_replace('"',"'",$record['help_text'])),
													'#id' => 'id_'.$record['tag'],
												);	
											}
											$customHtmlstart='';
    										
										//$customHtmlstart.='<input type="button" '.$requiredyesno.' value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-checkbox button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'consentcheckbox')==0)
										{
											if(strcmp($record['label'],'')!=0)
											{
												$form['name_'.$record['tag']] = array(
													'#type' => 'button',
													'#prefix' => $customHtmlstart,
													'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
													'#value' => $this->t($record['label']),
													'#attributes' => array('class'=>array('mailflatrate-label-insert-checkbox button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag'],'htHelpText'=>str_replace('"',"'",$record['help_text'])),
													'#id' => 'id_'.$record['tag'],
												);
											}
											else
											{
												$form['name_'.$record['tag']] = array(
													'#type' => 'button',
													'#prefix' => $customHtmlstart,
													'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
													'#value' => $this->t($record['tag']),
													'#attributes' => array('class'=>array('mailflatrate-label-insert-checkbox button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag'],'htHelpText'=>str_replace('"',"'",$record['help_text'])),
													'#id' => 'id_'.$record['tag'],
												);	
											}
 
										//$customHtmlstart.='<input type="button" '.$requiredyesno.' value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-checkbox button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'multiselect')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-multiselect button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);

										//$customHtmlstart.='<input type="button" '.$requiredyesno.' value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-multiselect button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';

									 $customHtmlstart='<div class="mailflatrate-multiselecthidden">\n<p> \n <label for="'.$record['tag'].'">'.$record['label'].' : </label> \n';
									 $customHtmlend='</p>
                                        </div>';
                                      foreach($record['options'] as $key => $option)
											{
												$options[$key] = $option;
												
											}

                                        	$form[$record['tag']] = array(
        												'#type' => 'select',
        												'#options' => $options,
														'#prefix' => $customHtmlstart,
														'#suffix' => $customHtmlend,
        												'#attributes' => array('class' => array("mailflatrate-label-dropdown ".(strcmp($record['required'],'yes')==0)?'required-class':''),'multiple'=>'multiple')
    										); 
    										$customHtmlstart='';
    										$customHtmlend='';
                        			
										}
										else if(strcmp($record['type']['identifier'],'date')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-date button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);

										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-date button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										 }
										else if(strcmp($record['type']['identifier'],'datetime')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-datetime button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-datetime button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										 }
										else if(strcmp($record['type']['identifier'],'textarea')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-textarea button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-textarea button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'country')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-country button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-country button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'state')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-state button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
										//$customHtmlstart.='<input .'.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-state button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'checkboxlist')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-checkboxlist button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);

										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-checkboxlist button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										$customHtmlstart='<div class="mailflatrate-checkboxlisthidden">\n<p> \n <label for="'.$record['tag'].'">'.$record['label'].' : </label> \n';
									 	$customHtmlend='</p>
                                        </div>';
                                          
						 
						 				foreach($record['options'] as $key => $option)
											{
												$form[$record['tag']] = array(
  												'#type' => 'checkbox',
  												'#prefix' => $customHtmlstart,
  												'#title' => $this->t($option),
  												'#id' => $record['tag'].'_'.$key,
  												'#attributes' => array('class'=>array((strcmp($record['required'],'yes')==0)?'required-class':''))
											);

											// $customHtmlstart.='<input type="checkbox" class="'.(strcmp($record['required'],'yes')==0)?'required-class':''.'" id="'.$record['tag'].'_'.$key.'" name="'.$record['tag'].'" value="'.$option.'"> <label for="'.$record['tag'].'_'.$key.'">'.$option.'</label>\n';
						  				}
						 				$customHtmlstart=$customHtmlend;
										}
										else if(strcmp($record['type']['identifier'],'radiolist')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-radiolist button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);

										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-radiolist button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';


                                        $customHtmlstart='<div class="mailflatrate-radiolisthidden">\n<p> \n <label>'.$record['label'].': </label> \n';
						 				$customHtmlend='</p>
                                        </div>';
										foreach($record['options'] as $key => $option)
											{
												$form[$record['tag']] = array(
  												'#type' => 'radios',
  												'#prefix' => $customHtmlstart,
  												'#title' => $this->t($option),
  												'#id' => $record['tag'].'_'.$key,
  												'#attributes' => array('class'=>(strcmp($record['required'],'yes')==0)?'required-class':''));
							 				 //$customHtmlstart.='<input type="radio" '.(strcmp($record['required'],'yes')==0)?'required-class':''.' id="'.$record['tag'].'_'.$key.'" name="'.$record['tag'].'" value="'.$option.'"> <label for="'.$record['tag'].'_'.$key.'">'.$option.'</label>\n';
						
                        					} 
                        				$customHtmlstart=$customHtmlend;
										}
									}
									}
								}
							
								/*$customHtmlstart.='<button class="button not-in-form submitbuttonadd" required="yes" type="button" value="0">Submit button</button>&nbsp;&nbsp;&nbsp;&nbsp;
								<button class="button not-in-form agreetoterms" type="button"  value="3">Agree to terms</button>';*/
								
							}
            		$customHtmlstart.='</div>
                </div>
            </div>';
			$customHtmlstart.='<div class="available-fields small-margin required"><h4>'.$this->t('Choose a field to add to the form').'</h4><div class="tiny-margin"><strong>'.$this->t('Required Form fields').'</strong><div class="buttons-fields-mailflatrate">';
			if(strcmp($config->get('mailflatrate.mailflatratelist'),'select')!=0 && strcmp($config->get('mailflatrate.mailflatratelist'),'')!=0)
							{
								$responseObject=getListFields($config->get('mailflatrate.mailflatratelist'));
								if(isset($responseObject->body['data']['records']))
								{
									$records=$responseObject->body['data']['records'];
									$requiredyes='yes';
									$requiredyesno='no';
									foreach($records as $record)
									{
										if(strcmp($record['required'],'yes')==0)
										{
											

										$requiredyesno=(strcmp($record['required'],'yes')==0)?'yes':'no';

										if((strcmp($record['type']['identifier'],'text')==0 || strcmp($record['type']['identifier'],'geocountry')==0 || strcmp($record['type']['identifier'],'geostate')==0) && strcmp($record['label'],'')!=0)
										{
										$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-text button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
										$customHtmlstart='';
										//$customHtmlstart.='<input type="button"'.$requiredyesno.' value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-text button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
								   }
								  		else if(strcmp($record['type']['identifier'],'dropdown')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-dropdown button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);

										//$customHtmlstart.='<input type="button" '.$requiredyesno.' value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-dropdown button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;

                                        $customHtmlstart='<div class="mailflatrate-dropdownhidden">\n<p> \n <label for="'.$record['tag'].'">'.$record['label'].' : </label> \n';
                                        $customHtmlend='</p>
                                        </div>';
                                        $options = array(
        									'select' => $this->t('Select')
    									);

                                        foreach($record['options'] as $key => $option)
											{
												$options[$key] = $option;
												
											}

                                        	$form[$record['tag']] = array(
        												'#type' => 'select',
        												'#options' => $options,
														'#prefix' => $customHtmlstart,
														'#suffix' => $customHtmlend,
        												'#attributes' => array('class' =>array( "mailflatrate-label-dropdown ".(strcmp($record['required'],'yes')==0)?'required-class':'')) 
    										);
    										$customHtmlstart='';
    										$customHtmlend='';
										 }
										else if(strcmp($record['type']['identifier'],'checkbox')==0)
										{
											if(strcmp($record['label'],'')!=0)
											{
												$form['name_'.$record['tag']] = array(
													'#type' => 'button',
													'#prefix' => $customHtmlstart,
													'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
													'#value' => $this->t($record['label']),
													'#attributes' => array('class'=>array('mailflatrate-label-insert-checkbox button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag'],'htHelpText'=>str_replace('"',"'",$record['help_text'])),
													'#id' => 'id_'.$record['tag'],
												);
											}
											else
											{
												$form['name_'.$record['tag']] = array(
													'#type' => 'button',
													'#prefix' => $customHtmlstart,
													'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
													'#value' => $this->t($record['tag']),
													'#attributes' => array('class'=>array('mailflatrate-label-insert-checkbox button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag'],'htHelpText'=>str_replace('"',"'",$record['help_text'])),
													'#id' => 'id_'.$record['tag'],
												);	
											}
											$customHtmlstart='';
    										
										//$customHtmlstart.='<input type="button" '.$requiredyesno.' value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-checkbox button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'consentcheckbox')==0)
										{
											if(strcmp($record['label'],'')!=0)
											{
												$form['name_'.$record['tag']] = array(
													'#type' => 'button',
													'#prefix' => $customHtmlstart,
													'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
													'#value' => $this->t($record['label']),
													'#attributes' => array('class'=>array('mailflatrate-label-insert-checkbox button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag'],'htHelpText'=>str_replace('"',"'",$record['help_text'])),
													'#id' => 'id_'.$record['tag'],
												);
											}
											else
											{
												$form['name_'.$record['tag']] = array(
													'#type' => 'button',
													'#prefix' => $customHtmlstart,
													'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
													'#value' => $this->t($record['tag']),
													'#attributes' => array('class'=>array('mailflatrate-label-insert-checkbox button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag'],'htHelpText'=>str_replace('"',"'",$record['help_text'])),
													'#id' => 'id_'.$record['tag'],
												);	
											}
 
										//$customHtmlstart.='<input type="button" '.$requiredyesno.' value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-checkbox button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'multiselect')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-multiselect button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);

										//$customHtmlstart.='<input type="button" '.$requiredyesno.' value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-multiselect button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';

									 $customHtmlstart='<div class="mailflatrate-multiselecthidden">\n<p> \n <label for="'.$record['tag'].'">'.$record['label'].' : </label> \n';
									 $customHtmlend='</p>
                                        </div>';
                                      foreach($record['options'] as $key => $option)
											{
												$options[$key] = $option;
												
											}

                                        	$form[$record['tag']] = array(
        										'#type' => 'select',
        										'#options' => $options,
												'#prefix' => $customHtmlstart,
												'#suffix' => $customHtmlend,
        										'#attributes' => array('class' => array("mailflatrate-label-dropdown ".(strcmp($record['required'],'yes')==0)?'required-class':''),'multiple'=>'multiple')
    										);
    										$customHtmlstart='';
    										$customHtmlend='';
                        			
										}
										else if(strcmp($record['type']['identifier'],'date')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-date button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);

										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-date button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										 }
										else if(strcmp($record['type']['identifier'],'datetime')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-datetime button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-datetime button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										 }
										else if(strcmp($record['type']['identifier'],'textarea')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-textarea button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-textarea button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'country')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-country button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-country button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'state')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-state button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);
										//$customHtmlstart.='<input .'.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-state button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										}
										else if(strcmp($record['type']['identifier'],'checkboxlist')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-checkboxlist button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);

										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-checkboxlist button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
										$customHtmlstart='<div class="mailflatrate-checkboxlisthidden">\n<p> \n <label for="'.$record['tag'].'">'.$record['label'].' : </label> \n';
									 	$customHtmlend='</p>
                                        </div>';
                                          
						 
						 				foreach($record['options'] as $key => $option)
											{
												$form[$record['tag']] = array(
  												'#type' => 'checkbox',
  												'#prefix' => $customHtmlstart,
  												'#title' => $this->t($option),
  												'#id' => $record['tag'].'_'.$key,
  												'#attributes' => array('class'=>array((strcmp($record['required'],'yes')==0)?'required-class':'')));

											// $customHtmlstart.='<input type="checkbox" class="'.(strcmp($record['required'],'yes')==0)?'required-class':''.'" id="'.$record['tag'].'_'.$key.'" name="'.$record['tag'].'" value="'.$option.'"> <label for="'.$record['tag'].'_'.$key.'">'.$option.'</label>\n';
						  				}
						 				$customHtmlstart=$customHtmlend;
										}
										else if(strcmp($record['type']['identifier'],'radiolist')==0 && strcmp($record['label'],'')!=0)
										{
											$form['name_'.$record['tag']] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t($record['label']),
  												'#attributes' => array('class'=>array('mailflatrate-label-insert-radiolist button not-in-form'),'data-required'=>$requiredyesno,'htname'=>$record['tag']),
  												'#id' => 'id_'.$record['tag'],
											);

										//$customHtmlstart.='<input '.$requiredyesno.' type="button" value="'.$record['label'].'" htname="'.$record['tag'].'" id="id_'.$record['tag'].'" class="mailflatrate-label-insert-radiolist button not-in-form" name="name_'.$record['tag'].'" />&nbsp;&nbsp;&nbsp;&nbsp;';
                                        $customHtmlstart='<div class="mailflatrate-radiolisthidden">\n<p> \n <label>'.$record['label'].': </label> \n';
						 				$customHtmlend='</p>
                                        </div>';
										foreach($record['options'] as $key => $option)
											{
												$form[$record['tag']] = array(
  													'#type' => 'radios',
  													'#prefix' => $customHtmlstart,
  													'#title' => $this->t($option),
  													'#id' => $record['tag'].'_'.$key,
  													'#attributes' => array('class'=>array((strcmp($record['required'],'yes')==0)?'required-class':'')));
							 				 //$customHtmlstart.='<input type="radio" '.(strcmp($record['required'],'yes')==0)?'required-class':''.' id="'.$record['tag'].'_'.$key.'" name="'.$record['tag'].'" value="'.$option.'"> <label for="'.$record['tag'].'_'.$key.'">'.$option.'</label>\n';
						
                        					} 
                        				$customHtmlstart=$customHtmlend;
										}
									
										}
									}
								}
										$form['submit'] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t('Submit'),
  												'#attributes' => array('class'=>array('button not-in-form submitbuttonadd'))); 
								//$customHtmlstart.='<button class="button not-in-form submitbuttonadd" required="yes" type="button" value="0">'.t('Submit').'</button>&nbsp;&nbsp;&nbsp;&nbsp;
								$customHtmlstart='';
								/*$form['agreetoterms'] = array(
  												'#type' => 'button',
  												'#prefix' => $customHtmlstart,
  												'#suffix' => '&nbsp;&nbsp;&nbsp;&nbsp;',
  												'#value' => $this->t('Agree to terms'),
  												'#attributes' => array('class'=>array('button not-in-form agreetoterms')));*/
								//'<button class="button not-in-form agreetoterms" type="button"  value="3">'.((strcmp($lang_name,'de')==0)?'Bedingungen zustimmen':'Agree to terms').'</button>';
								
							}
			$customHtmlstart.='</div>
                </div>
            </div>';
			$customHtmlstart.='</div>
			
			<div id="mailflatrate-field-wizard">
<div class="mailflatrate-row">
	<div class="mailflatrate-col mailflatrate-col-3 mailflatrate-form-editor-wrap">';
	
	$customHtmlend='</div>
</div>

<div class="mailflatrate-row">
	<h4 style="margin: 0"><label>'.$this->t('Preview').'</label></h4>
    <div class="subscribe" id="preview"></div>
</div>
</div></div></div></div></div>';
	$form['mailflatrate_form_code'] = array(
  				'#title' => $this->t('Form code'),
  				'#type' => 'textarea',
				'#prefix' => $customHtmlstart,
				'#suffix' => $customHtmlend,
  				'#description' => $this->t('Enter the HTML code for your form fields..'), 
  				'#default_value' => $config->get('mailflatrate.mailflatrate_form_code'),
  				'#rows' => 10,
  				'#cols' => 60, 
  				'#resizable' => TRUE,
			);
			}
		}
		
  $form['#attached']['library'][] = 'mailflatrate/settings-editor';
    return $form;
 
  }
  
 /* public function buildForm(array $form, FormStateInterface $form_state) {
 
    $form = parent::buildForm($form, $form_state);
 
    $config = $this->config('mailflatrate.settings');
 
    $form['email'] = array(
 
      '#type' => 'textfield',
 
      '#title' => $this->t('Email'),
 
      '#default_value' => $config->get('mailflatrate.email'),
 
      '#required' => TRUE,
 
    );
 
    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
 
    $node_type_titles = array();
 
    foreach ($node_types as $machine_name => $val) {
 
      $node_type_titles[$machine_name] = $val->label();
 
    }
 
    $form['node_types'] = array(
 
      '#type' => 'checkboxes',
 
      '#title' => $this->t('Node Types'),
 
      '#options' => $node_type_titles,
 
      '#default_value' => $config->get('mailflatrate.node_types'),
 
    );
 
    return $form;
 
  }
 */
  /**
 
   * {@inheritdoc}
 
   */
 
  public function submitForm(array &$form, FormStateInterface $form_state) {
 
    $config = $this->config('mailflatrate.editor_form');
 
    $config->set('mailflatrate.mailflatrate_form_code', $form_state->getValue('mailflatrate_form_code'));
	
	$config->set('mailflatrate.mailflatratelist', $form_state->getValue('mailflatratelist'));
 
    $config->save();
 
    return parent::submitForm($form, $form_state);
 
  }
 
  /**
 
   * {@inheritdoc}
 
   */
 
  protected function getEditableConfigNames() {
 
    return [
 
      'mailflatrate.editor_form',
 
    ];
 
  }
}