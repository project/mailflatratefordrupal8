<?php

namespace Drupal\mailflatrate\Plugin\Block;

use Drupal\Core\Block\BlockBase;


/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "mailflatrate_block",
 *   admin_label = @Translation("Mailflatrate"),
 * )
 */
 class MailflatrateBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
 public function build() {
 	$mailflatrate = new \Drupal\mailflatrate\MailflatrateForm();
  return [
    '#theme' => 'mailflatrateblock',
    '#mailflatrateEditorCode' => $mailflatrate->getCode(),
    '#termsagreeColor' => $mailflatrate->getAgreeTermsColor(),
    '#termsagree' => $mailflatrate->getAgreeTerms(),
    '#url' => base_path().'mailflatrate/submitform',
    '#attached' => [
        'library' => [
          'mailflatrate/settings-frontend',
        ],
      ],
  ];
}

}