<?php
/**
 * @file
 * Contains \Drupal\mailflatrate\SettingsController.
 */

namespace Drupal\mailflatrate;
 
use Drupal\Core\Form\ConfigFormBase;
 
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {
 /**  
   * {@inheritdoc}  
   */  
  public function getFormId() {
 
    return 'mailflatrate_config_form';
 
  }

  /**
 
   * {@inheritdoc}
 
   */
 
 public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
	$config = $this->config('mailflatrate.settings');
	$lang_name=\Drupal::languageManager()->getCurrentLanguage()->getId();
    $publickey  = $config->get('mailflatrate.manilflatrate_publickey');
	$privatekey  = $config->get('mailflatrate.manilflatrate_privatekey');
	$form['#attached']['library'][] = 'mailflatrate/settings-form';
		
	$Lists=getList($publickey,$privatekey,1,1);
	if(!isset($Lists->body['error']))
		{
	$form['manilflatrate_connected']=array(
	'#type' => 'fieldset',
	'#description'=>$this->t('Connected')
  );
		}
		else
		{
		$form['manilflatrate_connected_not']=array(
	'#type' => 'fieldset',
	'#description'=>$this->t('Not connected')
  );		
		} 
	$form['manilflatrate_publickey']=array(
	'#type' => 'textfield',
	'#title' => $this->t('Public Key'),
    '#description' => $this->t('Your Mailflatrate Public key'),
    '#required' => TRUE,
	'#default_value' => $config->get('mailflatrate.manilflatrate_publickey')
  );
  $form['manilflatrate_privatekey']=array(
	'#type' => 'textfield',
	'#title' => $this->t('Private Key'),
    '#description' => $this->t('Your Mailflatrate Private key'),
    '#required' => TRUE,
	'#default_value' => $config->get('mailflatrate.manilflatrate_privatekey')
  );
  
    return $form;
 
  }
  
 /* public function buildForm(array $form, FormStateInterface $form_state) {
 
    $form = parent::buildForm($form, $form_state);
 
    $config = $this->config('mailflatrate.settings');
 
    $form['email'] = array(
 
      '#type' => 'textfield',
 
      '#title' => $this->t('Email'),
 
      '#default_value' => $config->get('mailflatrate.email'),
 
      '#required' => TRUE,
 
    );
 
    $node_types = \Drupal\node\Entity\NodeType::loadMultiple();
 
    $node_type_titles = array();
 
    foreach ($node_types as $machine_name => $val) {
 
      $node_type_titles[$machine_name] = $val->label();
 
    }
 
    $form['node_types'] = array(
 
      '#type' => 'checkboxes',
 
      '#title' => $this->t('Node Types'),
 
      '#options' => $node_type_titles,
 
      '#default_value' => $config->get('mailflatrate.node_types'),
 
    );
 
    return $form;
 
  }
 */
  /**
 
   * {@inheritdoc}
 
   */
 
  public function submitForm(array &$form, FormStateInterface $form_state) {
 
    $config = $this->config('mailflatrate.settings');
 
    $config->set('mailflatrate.manilflatrate_publickey', $form_state->getValue('manilflatrate_publickey'));
	
	$config->set('mailflatrate.manilflatrate_privatekey', $form_state->getValue('manilflatrate_privatekey'));
 
    $config->save();
 
    return parent::submitForm($form, $form_state);
 
  }
 
  /**
 
   * {@inheritdoc}
 
   */
 
  protected function getEditableConfigNames() {
 
    return [
 
      'mailflatrate.settings',
 
    ];
 
  }
}