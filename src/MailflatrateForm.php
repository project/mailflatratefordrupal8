<?php
/**
 * @file
 * Contains \Drupal\resume\Form\WorkForm.
 */
namespace Drupal\mailflatrate;
 
use Drupal\Core\Form\FormBase;
 
use Drupal\Core\Form\FormStateInterface;

class MailflatrateForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mailflatrate_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $lang_name=\Drupal::languageManager()->getCurrentLanguage()->getId();;

   $config = $this->config('mailflatrate.editor_form');
   $configMessage = $this->config('mailflatrate.message_form');
    $mailflatrate_agree_to_term_color=$configMessage->get('mailflatrate.mailflatrate-agree_to_terms-color');
    if((strcmp($mailflatrate_agree_to_term_color,'')!=0))
    {
      $mailflatrate_agree_to_term_color=$configMessage->get('mailflatrate.mailflatrate-agree_to_terms-color');
    }
    else
    {
      $mailflatrate_agree_to_term_color='#000000';
    }

    $mailflatrate_agree_to_term=$configMessage->get('mailflatrate.mailflatrate-agree_to_terms');

    if((strcmp($mailflatrate_agree_to_term,'')!=0))
    {
      $mailflatrate_agree_to_term=$configMessage->get('mailflatrate.mailflatrate-agree_to_terms');
    }
    else
    {
      $mailflatrate_agree_to_term=$this->t('Please agree with terms and conditions');
    }

  $customHtmlstartList='';
  $customHtmlstartList.='<div class="mailflatrate-form">';
  $customHtmlstartList.='<div id="overlay"></div>';
  $customHtmlstartList.='<div class="messagebox"></div>';
  $form['submitmoduleurl'] = array(
              '#type' => 'hidden',
          '#prefix' => $customHtmlstartList, 
              '#default_value' => base_path().'/mailflatrate/submitform',
          '#attributes' => array('id'=>'submitmoduleurl') 
          );
  $form['mailflatrate-terms-agree-color'] = array(
              '#type' => 'hidden',
              '#default_value' => $mailflatrate_agree_to_term_color,
          '#attributes' => array('id'=>'mailflatrate-terms-agree-color') 
          );
  $form['mailflatrate-terms-agree'] = array(
              '#type' => 'hidden',
              '#default_value' => $mailflatrate_agree_to_term,
              '#attributes' => array('id'=>'mailflatrate-terms-agree') 
          );

   

    return $form;
  }
  public function getCode()
  {
     $config = $this->config('mailflatrate.editor_form');
    return $config->get('mailflatrate.mailflatrate_form_code');
  }
  public function getAgreeTermsColor()
  {
    $config = $this->config('mailflatrate.message_form');
     $mailflatrate_agree_to_term_color=$config->get('mailflatrate.mailflatrate-agree_to_terms-color');
    if((strcmp($mailflatrate_agree_to_term_color,'')!=0))
    {
      $mailflatrate_agree_to_term_color=$config->get('mailflatrate.mailflatrate-agree_to_terms-color');
    }
    else
    {
      $mailflatrate_agree_to_term_color='#000000';
    }
    return  $mailflatrate_agree_to_term_color;

  }
  public function getAgreeTerms()
  {
     $lang_name=\Drupal::languageManager()->getCurrentLanguage()->getId();
    $config = $this->config('mailflatrate.message_form');
     $mailflatrate_agree_to_term=$config->get('mailflatrate.mailflatrate-agree_to_terms');

    if((strcmp($mailflatrate_agree_to_term,'')!=0))
    {
      $mailflatrate_agree_to_term=$config->get('mailflatrate.mailflatrate-agree_to_terms');
    }
    else
    {
      $mailflatrate_agree_to_term=$this->t('Please agree with terms and conditions');
    }
    return $mailflatrate_agree_to_term;
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal\Core\Messenger\MessengerInterface::addMessage($this->t('@emp_name ,Your application is being submitted!', array('@emp_name' => $form_state->getValue('employee_name'))));
  }
  protected function getEditableConfigNames() {
 
    return [
 
      'mailflatrate.editor_form',
 
    ];
 
  }
}